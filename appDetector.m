function tVal= appDetector(x,y,t,Variant)
%UNTITLED2 Summary of this function goes here

% 1s
% RadRaw_LowPass_ChPt
% Eli_LowPass_ChPt
% RadRaw_movMean_100_ChPt
% Eli_movMean_100_ChPt
% RadRaw_movMean_50_ChPt
% Eli_movMean_50_ChPt
% RadRaw_decomp_ChPt
% Eli_decomp_ChPt
%
% RadRaw_LowPass_thresh
% Eli_LowPass_thresh
% RadRaw_movMean_100_thresh
% Eli_movMean_100_thresh
% RadRaw_movMean_50_thresh
% Eli_movMean_50_thresh
% RadRaw_decomp_thresh
% Eli_decomp_thresh

% sort Data
data = table();
data.x = x;
data.y = y;
data.t = t;
data = sortrows(data,"t",   "descend");

% split data for simple calculations
x_all = data.x;
y_all = data.y;
t_all = data.t;

clear data

% crop Data
aoi =t_all<-0.2 & ~isnan(x_all);
x = x_all(aoi);
y = y_all(aoi);
t = t_all(aoi);
clear aoi aoi2

% calc radial Distance
dCurve = sqrt(x.^2+y.^2);

% calc area of growing window starting with-0.2s going up until first approach data
for i = 1:size (x,1)
    covMatrix = cov(x(1:i), y(1:i));
    [V, D] = eig(covMatrix);
    V = abs(V);
    D = abs(D);
    aa = sqrt(D(1, 1));
    bb = sqrt(D(2, 2));
    aCurve (i) = pi * aa * bb;
end
aCurve = aCurve';

% create filter for lowpass filtering
fs = 100;  % Sampling frequency
fc = 0.5;   % Cutoff frequency
order = 3;  % Filter order
[b, a] = butter(order, fc/(fs/2), 'low');  % Design low-pass Butterworth filter


if Variant == "1s"
    tVal = -1;
elseif contains(Variant,"ChPt")

    % different Variants of Approach detector
    switch Variant

        case "RadRaw_LowPass_ChPt"
            % filter lowpass
            smoothed_Curve = filtfilt(b, a, dCurve);

        case "Eli_LowPass_ChPt"
            % filter lowpass
            smoothed_Curve = filtfilt(b, a, aCurve);

        case "RadRaw_movMean_100_ChPt"
            WINDOW = 100;
            smoothed_Curve = movmean(dCurve,WINDOW);

        case "Eli_movMean_100_ChPt"
            WINDOW = 100;
            smoothed_Curve = movmean(aCurve,WINDOW);
        case "RadRaw_movMean_50_ChPt"
            WINDOW = 50;
            smoothed_Curve = movmean(dCurve,WINDOW);
        case "Eli_movMean_50_ChPt"
            WINDOW = 50;
            smoothed_Curve = movmean(aCurve,WINDOW);
        case "RadRaw_decomp_ChPt"
            [LT,ST,R] = trenddecomp(dCurve);
            smoothed_Curve = LT + sum(ST,2);
        case "Eli_decomp_ChPt"
            [LT,ST,R] = trenddecomp(aCurve);
            smoothed_Curve = LT + sum(ST,2);
        case "RadRaw_rot_decomp_ChPt"

            subplot(2,4,1)
            plot(x,y)
            axis equal

            % define the dimension with most variance
            [~,dimax] = max(max(cov(x,y)));
            [~,dimin] = min(max(cov(x,y)));

            % round data
            xRounded = round(x,2);
            yRounded = round(y,2);
            dat = [xRounded,yRounded];

            % create linSpace Data over the dimension with the higher variance
            linSpaceDat = min(dat(:,dimax)):0.01:max(dat(:,dimax));

            % preallocate for speed
            unweightedDat = nan(size(linSpaceDat));

            % loop through positions along dimension with highest variance
            for lin = 1:numel(linSpaceDat)
                unweightedDat(lin) = median(dat(dat(:,dimax) == linSpaceDat(lin),dimin));
            end

            % remove nan values
            xx = linSpaceDat(~isnan(unweightedDat));
            yy = unweightedDat(~isnan(unweightedDat));

            % plot fit
            subplot(2,4,2)
            hold on
            if dimin == 1
                plot(unweightedDat,linSpaceDat,'.')
                plot(polyval(polyfit(xx,yy,1),linSpaceDat),linSpaceDat)
            else
                plot(linSpaceDat,unweightedDat,'.')
                plot(linSpaceDat,polyval(polyfit(xx,yy,1),linSpaceDat))
            end
            axis equal
            xline(0)

            % Fit a line to the data
            p = polyfit(xx,yy,1);

            % Extract the slope and intercept from the fitted line
            slope = p(1);
            intercept = p(2);

            % Calculate the angle to the x-axis
            angle_x = atan2(slope, 1);

            % Calculate the angle to the y-axis
            angle_y = atan2(1, slope);

            % Convert angles to degrees
            angle_x_degrees = rad2deg(angle_x);
            angle_y_degrees = rad2deg(angle_y);

            % create a matrix of these points
            v = [x,y]';

            % choose a point which will be the center of rotation
            x_center = mean(x);
            y_center = mean(y);

            % create a matrix which will be used later in calculations
            center = repmat([x_center; y_center], 1, length(x));

            % define a 60 degree counter-clockwise rotation matrix
            theta = angle_x;
            R = [cos(theta) -sin(theta); sin(theta) cos(theta)];

            % do the rotation...
            s = v - center;     % shift points in the plane so that the center of rotation is at the origin
            so = R*s;           % apply the rotation about the origin
            vo = so + center;   % shift again so the origin goes back to the desired center of rotation

            % pick out the vectors of rotated x- and y-data
            x_rotated = vo(1,:);
            y_rotated = vo(2,:);

            % make a plot
            subplot(2,4,3)
            plot(x, y, 'k-', x_rotated, y_rotated, 'r-', x_center, y_center, 'b*');
            axis equal

            % plot rotated coordinates over time
            subplot(2,4,4)
            plot(t,x_rotated)
            hold on
            plot(t,y_rotated)
            xlim([-15,0.5])
            xline(0)


            % filter lowpass
            if dimax == 1
                smoothed_Curve = filtfilt(b, a, x_rotated);
                dCurve = x_rotated;
            elseif dimax == 2
                smoothed_Curve = filtfilt(b, a, y_rotated);
                dCurve = y_rotated;
            end

            % plot the rotated smoothed curve of y over t
            subplot(2,4,5)
            hold on
             plot(t,smoothed_Curve)
            plot(t,dCurve)


            
  close all
    end


    % define optimal Num Changepoints
    MaxNumChanges_linear =  sum(ischange(smoothed_Curve,'linear'));
    MaxNumChanges_mean =  sum(ischange(smoothed_Curve,'mean'));
    MaxNumChanges_var =  sum(ischange(smoothed_Curve,'variance'));

   [changePoints_linear, segmentDistances_linear] = findchangepts(smoothed_Curve,'Statistic','linear','MaxNumChanges',MaxNumChanges_linear);
   [changePoints_mean, segmentDistances_mean] = findchangepts(smoothed_Curve,'Statistic','mean','MaxNumChanges',MaxNumChanges_mean);
   [changePoints_var, segmentDistances_var] = findchangepts(smoothed_Curve,'Statistic','std','MaxNumChanges',MaxNumChanges_var);

   
   
   % determine number of segments
    numSegments = numel(changePoints_linear) + 1;

    % initialize table segments
    segments= table();

    % iterate over each segment and calc linear components
    for i = 1:numSegments
        if i == 1
            segmentData = smoothed_Curve(1:changePoints_linear(i));
            segments.start(i) = 1;
            segments.stop(i) = changePoints_linear(i);
        elseif i == numSegments
            segmentData = smoothed_Curve(changePoints_linear(i-1)+1:end);
            segments.start(i)= changePoints_linear(i-1)+1;
            segments.stop(i) = numel(smoothed_Curve);
        else
            segmentData = smoothed_Curve(changePoints_linear(i-1)+1:changePoints_linear(i));
            segments.start(i)=  changePoints_linear(i-1)+1;
            segments.stop(i) = changePoints_linear(i);
        end

        % Fit a linear model to the segment data
        coeffs = polyfit(segments.start(i):segments.stop(i), segmentData, 1);

        % Extract the constant and slope coefficients
        slope = coeffs(1);
        constant = coeffs(2);

        % assign constant and slope to segments
        segments.constant(i) = abs(constant);
        segments.meanDat(i) = abs(mean(segmentData));
        segments.slope(i) = abs(slope);
        % figure(2)
        % hold on
        % plot(segments.start(i):segments.stop(i),segmentData,'g')
        % xline(segments.start(i))
        % xline(segments.stop(i))
        clear segmentData coeffs constant slope

    end
    figure(1)
title('linear')
findchangepts(smoothed_Curve,"Statistic","linear", "MaxNumChanges",MaxNumChanges_linear)
figure(2)
title('mean')
findchangepts(smoothed_Curve,"Statistic","mean", "MaxNumChanges",MaxNumChanges_mean)
figure(3)
title('variance')
findchangepts(smoothed_Curve    ,"Statistic","std","MaxNumChanges",  MaxNumChanges_var)

close all
% sortSegments
    segments = sortrows(segments,{'slope'},'ascend');
    segments.slopeRank= [1:size(segments,1)]';
    segments = sortrows(segments,{'meanDat'},'ascend');
    segments.meanDatRank= [1:size(segments,1)]';
    segments = sortrows(segments,{'start'},'ascend');
    segments.posRank = [1:size(segments,1)]';
    segments.rank = segments.meanDatRank + segments.slopeRank + segments.posRank;

   
X = zscore(segments{:,3:5});

% Set a range for the number of clusters you want to try out
numClusters = 2:numel(segments.rank)-1;
silhouettes = zeros(length(numClusters),1);

% Compute silhouette values for each number of clusters
for k = numClusters
    clusterIDs = kmeans(X, k);
    silh = silhouette(X, clusterIDs);
    silhouettes(k-1) = mean(silh);
end
% Plot the silhouette curve
figure
plot(numClusters, silhouettes)
title('Silhouette Method')
xlabel('Number of Clusters')
ylabel('Average Silhouette Value')

[~,optNumClusters] = max(silhouettes);
optNumClusters = optNumClusters + 1;

% cluster segments
    [cidx2,cmeans2] = kmeans(X,optNumClusters,'dist','sqeuclidean');

    % assing clusterid to segmentTable
    segments.Cluster = cidx2;

 % find minimum slope and minimum segment mean
    [~,id]= min(segments.rank);

    % select "hold" cluster
    segments = segments(segments.Cluster == segments.Cluster(id),:);

    % find start Stop of approach
    tVal = t(max(segments.stop));

elseif contains(Variant,"thresh")
    switch Variant
        case "RadRaw_LowPass_thresh"
            % filter lowpass
            smoothed_Curve = filtfilt(b, a, dCurve);
            rawCurve = dCurve;

        case "Eli_LowPass_thresh"
            % filter lowpass
            smoothed_Curve = filtfilt(b, a, aCurve);
            rawCurve = aCurve;

        case "RadRaw_movMean_100_thresh"
            WINDOW = 100;
            smoothed_Curve = movmean(dCurve,WINDOW);
            rawCurve = dCurve;
        case "Eli_movMean_100_thresh"
            WINDOW = 100;
            smoothed_Curve = movmean(aCurve,WINDOW);
            rawCurve = aCurve;
        case "RadRaw_movMean_50_thresh"
            WINDOW = 50;
            smoothed_Curve = movmean(dCurve,WINDOW);
            rawCurve = dCurve;
        case "Eli_movMean_50_thresh"
            WINDOW = 50;
            smoothed_Curve = movmean(aCurve,WINDOW);
            rawCurve = aCurve;
        case "RadRaw_decomp_thresh"
            [LT,ST,R] = trenddecomp(dCurve);
            smoothed_Curve = LT + sum(ST,2);
            rawCurve = dCurve;
        case "Eli_decomp_thresh"
            [LT,ST,R] = trenddecomp(aCurve);
            smoothed_Curve = LT + sum(ST,2);
            rawCurve = aCurve;
    end

    dTrended_Curve = abs(detrend(rawCurve,'omitnan'));
    threshold = nanmedian(dTrended_Curve) + 0.5*nanstd(dTrended_Curve);

    approachStopIdx = [];
    c = 0;
    counter = 0;
    while isempty(approachStopIdx)
        counter = counter +1;
        c = c+0.01;
        threshold = threshold + c;
        approachStopIdx = find(threshold>smoothed_Curve,1,'last');

        if counter > 10000
            approachStopIdx = t == -1;
            break
        end
    end
    tVal = t(approachStopIdx);

    % plot solution
plot (dTrended_Curve)
hold on
yline(threshold)
plot (smoothed_Curve)
title(strrep(Variant,"_","  "))
xline(approachStopIdx)
% pause(1)
close all
end

% plot solution
if Variant ~= "1s"
    figure('units','normalized','outerposition',[0 0 1 1])
    subplot(1,2,1)
    hold on
    plot (t,dCurve,'k')
    plot (t,aCurve,'r')
    plot(t,smoothed_Curve, 'b','LineWidth',2)
    xline(tVal,'c','LineWidth',2)
    legend('dCurve','aCurve','smoothed','approachStop')
    title(strrep(Variant,"_"," "))
    grid on

    if exist ('threshold') == 1
        yline(threshold)
    end
 
end

end









